export ARCHIVE=/backup/dental-conseil_$(date +%Y%m%d_%H%M).archive

mongodump --host ${MONGO_HOST} --ssl --username ${MONGO_USER} --password ${MONGO_PASS} --authenticationDatabase admin --db ${MONGO_DATABASE} --archive > ${ARCHIVE}

aws s3 cp ${ARCHIVE} s3://${S3_BUCKET}